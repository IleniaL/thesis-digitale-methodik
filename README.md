# Harmonisation, management and translation of metadata in digital project

Data management, curation and harmonisation are fundamental for a befitting categorisation and canonisation of digital objects. Digital projects heavily rely on the completeness and accuracy of meta-descriptions in order to make their materials findable and usable. Furthermore, the relevance of multilinguality in the digital information space was portrayed. Multilingual projects not only gain more visibility, but they also let knowledge flourish by tearing down linguistical barriers.

This thesis focuses on the harmonisation, management and translation of metadata records and aims at evaluating the automatic translation of metadata records using BabelNet, a multilingual encyclopedic dictonary.

It is apparent that the implementation of a multilingual dataset, interface and query options increases the visibility, promotes the propagation and enhances the overall outlook of digital repositories. Such implementation in websites and digital projects is widely spread: well-known examples are Wikipedia and the Gutenberg Project. However, a general and standardised workflow to accelerate the translation processes is unaccounted. Moreover, the evaluation of automatic translation tools is missing, especially with regards to harmonised and unharmonised data.

For this reason, this thesis' scope is to automatically translate metadata records and to compare and evaluate not only the translation tool chosen, but also the differences between harmonised and unharmonised datasets. 

## This repository (folder structure):

In this repository, there are 3 main folders: 

- "code": where you can find the Java classes used to translate the data with the BabelNet Java API.
- "data": where the data used in this project is saved (does not contain the raw data).
- "evaluation": with the tables for the evaluation of the data.

Additionally, you can find the Poster and the pdf version of the thesis.


## The Work
### Introduction

The translation of information in multiple languages assures faster and higher proliferation of knowledge. Automatic translation strategies have been implemented in multiple ways, from the well known multilingual \textit{Wikipedia} to the more recent automatic translation of social media posts.
Multilingual metadata can increase not only the visibility and approachability of webpages and digital projects, but can also grant greater overall information access for worldwide users, independently from their linguistical background.
However, although automatic translation methods are being implemented in different areas, the research on multilingual metadata and the methodologies and standards to create and harvest such metadata are spare. 


Moreover, there are general difficulties in the definition and the evaluation of metadata quality. Comprehensive evaluation strategies are lacking since the prime definition of qualitative metadata is quite vague. In addition, metadata records are often harvested and curated by non professional indexers. The role of the \ac{rdm} or of metadata experts is extremely valuable for the successful harmonisation of metadata. Without the harmonisation and normalisation of the records, the digital objects and resources will be disorganized and difficult to retrieve. As a consequence, unharmonised metadata will perform worse when generating automatic translations. 

For these reasons, the experimentation and analysis of ways for an optimised automatic translation of metadata records is indubitably influential in the digital era.
This thesis' research question is therefore twofold: firstly, how multilingual metadata can be automatically and effortlessly generated and secondly how harmonised and unharmonised metadata behaves when translated automatically.


### Aims and Objectives

The main objective of this thesis is to automatically translate metadata records into multiple languages. In addition, the differences between the translations of harmonised and unharmonised metadata records will be compared and evaluated. 

2 datasets have been translated: the [Mansi3](http://www.fscire.it/index.php/it/ricerca/mansi/) and the [RETOPEA](http://retopea.eu/) using [BabelNet](http://babelnet.org) Java API.

The code and evaluation of results can be found in the according folders.
