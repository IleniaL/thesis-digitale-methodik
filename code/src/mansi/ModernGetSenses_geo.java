package mansi;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import it.uniroma1.lcl.babelnet.BabelNet;
import it.uniroma1.lcl.babelnet.BabelNetQuery;
import it.uniroma1.lcl.babelnet.BabelSense;
import it.uniroma1.lcl.babelnet.BabelSynset;
import it.uniroma1.lcl.babelnet.BabelSynsetID;
import it.uniroma1.lcl.babelnet.data.BabelDomain;
import it.uniroma1.lcl.babelnet.data.BabelSenseSource;
import it.uniroma1.lcl.jlt.util.Language;
import it.uniroma1.lcl.kb.Domain;

public class ModernGetSenses_geo {
	public static void geoRestriction(String word, Language ... languages) throws IOException {
		BabelNet bn = BabelNet.getInstance();

		BabelNetQuery query = new BabelNetQuery.Builder(word)
				.to(languages)
				.build();

		BabelDomain domain = BabelDomain.GEOGRAPHY_AND_PLACES;
		List<Language> allowedLanguages = Arrays.asList(languages);
		
		List<BabelSynset> synsets = bn.getSynsets(query);
    	List<BabelSynset> geoFilter = new ArrayList<BabelSynset>();
    	
    	for (BabelSynset synset : synsets) {
    		HashMap<Domain, Double> synsetDomains = synset.getDomains();
    		if(synsetDomains.containsKey(domain)){
        		geoFilter.add(synset);
        	}
    	}
    	
    	System.out.println("TRANSLATIONS FOR " + word + " ###");
    	
    	for (BabelSynset geoSynset : geoFilter) {
    		if (geoFilter.size() <= 1) {
        		for (Language language : allowedLanguages)	{
        			if (allowedLanguages.contains(language)) {
        				System.out.println(geoSynset.getSenses(language, BabelSenseSource.WIKIDATA).toString());
        			}
        		}
    		} else if (geoFilter.size() > 1) {
    			List<BabelSynset> multiSynset = new ArrayList<BabelSynset>();
    			Map<BabelSynsetID, Integer> mapIDs = new HashMap<BabelSynsetID, Integer>();
    			
				for (BabelSynset synset : geoFilter) {
					multiSynset.add(synset);					// Here the 4 synsets for "Turin" are saved
				}
				
				for (BabelSynset synset2 : multiSynset) {				// For each synset in multiSynset(=4) get Senses
					
					List<BabelSense> senses = new ArrayList<BabelSense>();
					
					for (Language language : allowedLanguages)	{
	        			if (allowedLanguages.contains(language)) {
	        				senses.addAll(synset2.getSenses(language, BabelSenseSource.WIKIDATA));
	        			}
	        		}

					List<Language> countLang = new ArrayList<Language>();
					for (BabelSense sense : senses) {
						Language senseLang = sense.getLanguage();
						countLang.add(senseLang);			
					}
					mapIDs.put(synset2.getID(), countLang.size());
				}
				
				Map.Entry<BabelSynsetID, Integer> maxEntry = null;
				for (Map.Entry<BabelSynsetID, Integer> entry : mapIDs.entrySet()) {
					if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
			            maxEntry = entry;
			        }
				}
				
				BabelSynset singleSynset = maxEntry.getKey().toSynset();
				
    			for (Language language : allowedLanguages)	{
        			if (allowedLanguages.contains(language)) {
        				System.out.println(singleSynset.getSenses(language, BabelSenseSource.WIKIDATA).toString());
        			}
    			}
    			}
    		break;
    	}
   		System.out.println("---------------");
	}    	
	
	public static void main(String[] args) throws IOException {
		String file = "/roedel/ilaudito/BabelNet-API-4.0/data/mansi-modern.txt";
		BufferedReader br = new BufferedReader(new FileReader(file));
		String word = null;
		
		Set<Language> languages = new HashSet<Language>();
		languages.add(Language.DE);
		languages.add(Language.EN);
		languages.add(Language.ES);
		languages.add(Language.FR);
		languages.add(Language.IT);
		languages.add(Language.JA);
		languages.add(Language.LA);
		languages.add(Language.PT);
		
		while ((word = br.readLine()) != null) {
			geoRestriction(word, languages.toArray(new Language[languages.size()]));
			System.out.println("=================================================");
		}
		br.close();
	}
}
