package retopea;

import it.uniroma1.lcl.babelnet.BabelNet;
import it.uniroma1.lcl.babelnet.BabelNetQuery;
import it.uniroma1.lcl.babelnet.BabelSense;
import it.uniroma1.lcl.babelnet.BabelSynset;
import it.uniroma1.lcl.babelnet.data.BabelDomain;
import it.uniroma1.lcl.jlt.util.Language;
import it.uniroma1.lcl.jlt.util.ScoredItem;
import it.uniroma1.lcl.jlt.util.Triple;
import it.uniroma1.lcl.kb.Domain;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

public class UnharmoGetTranslations_geo {
	public static Multimap<Language, ScoredItem<String>> getTranslations(String word, Language language, List<BabelSynset> synsets, List<Language> languages) throws IOException {
		Map<Language, HashMap<String, Integer>> traslationCounters = new HashMap<Language,HashMap<String, Integer>>();

		for (BabelSynset synset : synsets) {
			// helps considering only once multiple source-target pairs in a specific language with the same lemma (e.g., overblown redirections)
			Set<Triple<String, String, Language>> translationsDone = new HashSet<Triple<String, String, Language>>();
			
			Multimap<BabelSense, BabelSense> translations = synset.getTranslations();
			List<BabelSense> senses = synset.getSenses();
			
			for (BabelSense sense : senses) {
				String senseLemma = sense.getSimpleLemma().toLowerCase();
				Collection<BabelSense> senseTranslations = translations.get(sense);
				
				for (BabelSense senseTranslation : senseTranslations) {
					Language translationLanguage = senseTranslation.getLanguage();
					if (translationLanguage == language) continue;
					String translation = senseTranslation.getSimpleLemma().toLowerCase();

					Triple<String, String, Language> translationTriple = 
							new Triple<String, String, Language>(senseLemma, translation, translationLanguage);
					if (translationsDone.contains(translationTriple)) continue;
					translationsDone.add(translationTriple);

					// valid translation: count it!
					HashMap<String, Integer> translationCounter = traslationCounters.get(translationLanguage);
					if (translationCounter == null) {
						translationCounter = new HashMap<String, Integer>();
						traslationCounters.put(translationLanguage, translationCounter);
					}
					
					Integer c = translationCounter.get(translation);
					if(c == null) {
						c = 0;
					}					
					c++;
					translationCounter.put(translation, c);
				}
			}
		}
		
		Multimap<Language, ScoredItem<String>> babelTraslations = HashMultimap.create();
		
		for (Language otherLanguage : traslationCounters.keySet()) {
			HashMap<String, Integer> translationCounter = traslationCounters.get(otherLanguage);
			for (String translation : translationCounter.keySet()) {
				babelTraslations.put(otherLanguage, new ScoredItem<String>(translation, translationCounter.get(translation)));
			}
		}
		return babelTraslations;
	}
	
	public static void geoRestriction(List<BabelSynset> wordSynset, String word, Language languageToSearch, Language... languagesToPrint) throws IOException {
		BabelDomain domain = BabelDomain.GEOGRAPHY_AND_PLACES;
		
		ArrayList<BabelSynset> geoFilter = new ArrayList<BabelSynset>();
    	
    	for (BabelSynset synset : wordSynset) {
        	HashMap<Domain, Double> synsetDomains = synset.getDomains();
        	
        	if(synsetDomains.containsKey(domain)){	
        		geoFilter.add(synset);
        	}
        }
    	
    	List<Language> allowedLanguages = Arrays.asList(languagesToPrint);
    	Multimap<Language, ScoredItem<String>> translations = getTranslations(word, languageToSearch, geoFilter, allowedLanguages);
    	
    	for (Language language : translations.keySet())	{
			if (allowedLanguages.contains(language)) {
				System.out.println(language + ";" + translations.get(language));
			}
		}
    	System.out.println("---------------");
	}
	
	public static void main(String[] args) throws IOException {
		String file = "/roedel/ilaudito/BabelNet-API-4.0/data/retoUnharmo-spatial.txt";
		BabelNet bn = BabelNet.getInstance();
		BufferedReader br = new BufferedReader(new FileReader(file));
		String word = null;
		Language lang = Language.EN;
		

    	while ((word = br.readLine()) != null) {
    		BabelNetQuery query = new BabelNetQuery.Builder(word)
    				.from(lang)
    				.build();
        	
        	List<BabelSynset> wordSynset = bn.getSynsets(query);
        	//ListIterator<BabelSynset> wordIterator = wordSynset.listIterator();
        	
    		List<Language> languages = new ArrayList<Language>();
    		languages.add(Language.DE);
    		languages.add(Language.ES);
    		languages.add(Language.FR);
    		languages.add(Language.IT);
    		languages.add(Language.JA);
    		languages.add(Language.LA);
    		languages.add(Language.PT);
    		
        	geoRestriction(wordSynset, word, lang, languages.toArray(new Language[languages.size()]));
            System.out.println("=================================================");
    	}
		br.close();
	}
}
