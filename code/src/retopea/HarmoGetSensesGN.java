package retopea;

import it.uniroma1.lcl.babelnet.BabelNet;
import it.uniroma1.lcl.babelnet.BabelNetQuery;
import it.uniroma1.lcl.babelnet.BabelSense;
import it.uniroma1.lcl.babelnet.BabelSynset;
import it.uniroma1.lcl.babelnet.BabelSynsetID;
import it.uniroma1.lcl.babelnet.data.BabelSenseSource;
import it.uniroma1.lcl.jlt.util.Language;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HarmoGetSensesGN {
	public static void getSenses(String word, Language ... languages) throws IOException {
		BabelNet bn = BabelNet.getInstance();

		BabelNetQuery query = new BabelNetQuery.Builder(word)
				.from(Language.EN)
				.to(languages)
				.build();

		List<Language> allowedLanguages = Arrays.asList(languages);
		
		List<BabelSynset> synsets = bn.getSynsets(query);
    	List<BabelSynset> geoFilter = new ArrayList<BabelSynset>();
    	
    	System.out.println("TRANSLATIONS FOR " + word + " ###");
    	
    	for (BabelSynset synset : synsets) {
    		if (geoFilter.size() <= 1) {
        		for (Language language : allowedLanguages)	{
        			if (allowedLanguages.contains(language)) {
        				System.out.println(synset.getSenses(language, BabelSenseSource.GEONM).toString());
        			}
        		}
        		
    		} else if (geoFilter.size() > 1) {
    			List<BabelSynset> multiSynset = new ArrayList<BabelSynset>();
    			Map<BabelSynsetID, Integer> mapIDs = new HashMap<BabelSynsetID, Integer>();
    			
    			// save the multiple synsets
    			for (BabelSynset synset1 : geoFilter) {
    				multiSynset.add(synset1);					
    			}
    			
    			// For each synset in multiSynset getSenses
    			for (BabelSynset synset2 : multiSynset) {				
    				List<BabelSense> senses = new ArrayList<BabelSense>();
    				
    				for (Language language : allowedLanguages)	{
            			if (allowedLanguages.contains(language)) {
            				senses.addAll(synset2.getSenses(language, BabelSenseSource.GEONM));
            			}
            		}    	

    				List<Language> countLang = new ArrayList<Language>();
    				for (BabelSense sense : senses) {
    					Language senseLang = sense.getLanguage();
    					countLang.add(senseLang);			
    				}
    				mapIDs.put(synset2.getID(), countLang.size());
    			}
    			
    			Map.Entry<BabelSynsetID, Integer> maxEntry = null;
    			for (Map.Entry<BabelSynsetID, Integer> entry : mapIDs.entrySet()) {
    				if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
    		            maxEntry = entry;
    		        }
    			}
    			
    			BabelSynset singleSynset = maxEntry.getKey().toSynset();
    			
    			for (Language language : allowedLanguages)	{
        			if (allowedLanguages.contains(language)) {
        				System.out.println(singleSynset.getSenses(language, BabelSenseSource.GEONM).toString());
        			}
    			}
    			}
    		break;
    	}
   		System.out.println("---------------");
	}    	

	public static void main(String[] args) throws IOException {
		String file = "/roedel/ilaudito/BabelNet-API-4.0/data/retoHarmo-spatial.txt";
		BufferedReader br = new BufferedReader(new FileReader(file));
		String word = null;
		
		List<Language> languages = new ArrayList<Language>();
		languages.add(Language.DE);
		languages.add(Language.ES);
		languages.add(Language.FR);
		languages.add(Language.IT);
		languages.add(Language.JA);
		languages.add(Language.LA);
		languages.add(Language.PT);
		
		while ((word = br.readLine()) != null) {
			getSenses(word, languages.toArray(new Language[languages.size()]));
			System.out.println("=================================================");
		}
		br.close();
	}
}